const API_URL = 'https://bsa-node-js.herokuapp.com/';

function callApi(endpoind, { method, headers, body }) {
  const url = API_URL + endpoind;
  const options = {
    method,
    headers,
    body
  };

  return fetch(url, options)
    .then(response =>
      response.ok ? response.json() : Promise.reject(Error('Failed to load'))
    )
    .catch(error => {
      throw error;
    });
}

export { callApi }