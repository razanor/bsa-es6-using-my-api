import { callApi } from '../helpers/apiHelper';

class FighterService {
  async getFighters() {
    try {
      const endpoint = 'fighters';
      const apiResult = await callApi(endpoint, { method: 'GET'});
      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(_id) {
    try {
      const endpoint = `fighters/${_id}`;
      const apiResult = await callApi(endpoint, { method: 'GET' });
      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async updateFighterDetails(_id, data) {
    try {
      const endpoint = `fighters/${_id}`;
      data._id = undefined;
      const body = JSON.stringify(data);  
      await callApi(endpoint, {
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body
      });
    } catch(error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
